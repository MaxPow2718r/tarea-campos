import matplotlib.pyplot as plt
import numpy as np

# Campo
def E(q, r, x, y, epsilon_relativo):
    den = 4 * np.pi * epsilon_relativo * 8.854e-12 * np.sqrt((x - r[0])**2 + (y - r[1])**2) **3
    Efield = q * (x - r[0]) / den, q * (y - r[1]) / den
    return Efield

# Fuerza
def F(q, E):
    Fx = q * E[0]
    Fy = q * E[1]
    return Fx, Fy

# Dimensiones
l      = 0.5
a      = 0.5
nx, ny = 100, 100
x      = np.linspace(0, l, nx)
y      = np.linspace(0, a, nx)
X, Y   = np.meshgrid(x, y)

# Cargas y posiciones
q1 = 7e-8
r1 = [0.1, 0.1]
q2 = -5e-8
r2 = [0.15, 0.35]
q3 = 3e-8
r3 = [0.4,0.2]
q4 = 5e-8
r4 = [0.25,0.25]

cargas    = [(q1, (r1)), (q2, (r2)), (q3, (r3)), (q4, (r4))]
cargas_q4 = [(q1, (r1)), (q2, (r2)), (q3, (r3))]

# Calculo de campos
Ex, Ey   = np.zeros((nx, ny)), np.zeros((nx, ny))
for i in cargas:
    ex, ey = E(*i, x = X, y = Y, epsilon_relativo = 1)
    Ex += ex
    Ey += ey

# Calculo de campo sobre la carga q4
permitividades = [1, 24.3, 80.2]
Fq = []
for j in permitividades:
    Exq4, Eyq4     = np.zeros((nx, ny)), np.zeros((nx, ny))
    for i in cargas_q4:
        exq4, eyq4 = E(*i, x = r4[0], y = r4[1], epsilon_relativo = j)
        Exq4 += exq4
        Eyq4 += eyq4

    # Fuerza sobre la carga
    px       = int (r4[0] * nx)
    py       = int (r4[0] * ny)
    E_totalx = Exq4[px, py]
    E_totaly = Eyq4[px, py]
    E_carga  = [E_totalx, E_totaly]
    Fuerza   = F(q4, E_carga)
    Fq.append(Fuerza)

F_vacio  = Fq[0]
F_etanol = Fq[1]
F_agua   = Fq[2]
with open("output_vacio.tex", "w") as tex:
    print(f"F_x &= {F_vacio[0]} \\\\ \nF_y &= {F_vacio[1]}", file=tex)
with open("output_etanol.tex", "w") as tex:
    print(f"F_x &= {F_etanol[0]} \\\\ \nF_y &= {F_etanol[1]}", file=tex)
with open("output_agua.tex", "w") as tex:
    print(f"F_x &= {F_agua[0]} \\\\ \nF_y &= {F_agua[1]}", file=tex)

# Grafico
color = 2 * np.log(np.hypot(Ex,Ey))
plt.streamplot(x, y, Ex, Ey, color = color, cmap = plt.cm.inferno, density = 2,
        arrowstyle = '->', arrowsize = 1.5)
#plt.contour(X, Y, np.hypot(Ex, Ey))
color_cargas = {True: '#FF0000', False: '#0000FF'}
fig, ax = plt.gcf(), plt.gca()
plt.xlim(0,0.5)
plt.ylim(0,0.5)
plt.xlabel("Ancho $x$ en metros")
plt.ylabel("Alto $y$ en metros")
for q, pos in cargas:
    car = plt.Circle(pos, 0.01, color = color_cargas[q>0])
    ax.add_artist(car)


plt.show()
