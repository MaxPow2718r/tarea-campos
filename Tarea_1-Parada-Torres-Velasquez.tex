\documentclass[conference]{IEEEtran}

%encodec type
\usepackage[utf8]{inputenc}
\usepackage[spanish,es-tabla]{babel}
\decimalpoint

%packages for links in the document
\usepackage[pdftex,colorlinks=true, urlcolor=blue, linkcolor = black, citecolor=black]{hyperref}

%math fonts
\usepackage{amsmath, amsfonts, amssymb}

\usepackage[backend=biber]{biblatex}
\usepackage{csquotes}
\addbibresource{ref.bib}
%more packages <|++|>
\usepackage{siunitx}
\usepackage{tikz}
\usepackage{graphicx}

\title{Simulación de la teoría electromagnética}

\author{\IEEEauthorblockN{Hugo Parada}
\IEEEauthorblockA{\textit{Instituto de Electricidad y Electrónica} \\
\textit{Universidad Austral de Chile} \\
Valdivia, Chile \\
\href{mailto:hugo.parada01@alumnos.uach.cl}{hugo.parada01@alumnos.uach.cl} }
\and
\IEEEauthorblockN{Gonzalo Torres}
\IEEEauthorblockA{\textit{Instituto de Electricidad y Electrónica} \\
\textit{Universidad Austral de Chile} \\
Valdivia, Chile \\
\href{mailto:gonzalo.torres@alumnos.uach.cl}{gonzalo.torres@alumnos.uach.cl} }
\and
\IEEEauthorblockN{Pablo Velásquez}
\IEEEauthorblockA{\textit{Instituto de Electricidad y Electrónica} \\
\textit{Universidad Austral de Chile} \\
Valdivia, Chile \\
\href{mailto:pablo.velasquez01@alumnos.uach.cl}{pablo.velasquez01@alumnos.uach.cl}  }
}

\begin{document}
\maketitle
\begin{abstract}
En el presente documento se contemplará el análisis y estudio de campos eléctricos en dos distintos
fenómenos físicos, la interacción de cargas puntuales en distintos medios y un condensador de placas
paralelas con dieléctricos variados, abordando leyes físicas como la ley de Coulomb o la ley de Gauss
y conceptos como campo eléctrico y capacitancia.
\end{abstract}

\begin{IEEEkeywords}
Electromagnético, electrostática, campo, QuickField, ecuaciones de Maxwell, Ley de Coulomb, capacitancia.
\end{IEEEkeywords}
\section{Introducción}

Los fenómenos electromagnéticos estudian las interacciones entre el campo eléctrico y el magnético. Todos estos,
pueden ser descritos por un grupo de ecuaciones. A su vez, estas ecuaciones pueden desacoplarse y entregarnos
dos pares, los cuales por si solos describen estos fenómenos por separado.

El estudio de el campo eléctrico y la electrostática, será el principal objetivo de este documento.
Se espera poder comparar los resultados calculados de manera analítica con simulaciones.


\section{Marco teórico}

\begin{align}
		\vec{\nabla} \bullet \vec{D} &= \rho_v \\
		\vec{\nabla} \times \vec{E} &= - \frac{\partial \vec{B}}{\partial t} \\
		\vec{\nabla} \bullet \vec{B} &= 0 \\
		\vec{\nabla} \times \vec{H} &= \vec{J} + \frac{\partial \vec{D}}{\partial t}
\end{align}

Los fenómenos electromagnéticos pueden ser descritos por las ecuaciones de Maxwell. Con este set de cuatro
ecuaciones es posible describir cualquier fenómeno que involucre estos campos. El objetivo particular de este
documento es explicar fenómenos relacionados con la electrostática. Estos pueden ser explicados si las
derivadas temporales en las ecuaciones de Maxwell se hacen cero, con ello, se pueden desacoplar las 4
ecuaciones en dos pares de ecuaciones que por separados explican los fenómenos electrostáticos y
magnetostáticos\footnote{De aquí en adelante los vectores serán representados con una flecha sobre
la letra correspondiente.}.

\begin{align}
		\vec{\nabla} \bullet \vec{D} &= \rho_v \label{eq:gauss} \\
		\vec{\nabla} \times \vec{E} &= 0 \label{eq:faradayestatica}
\end{align}

El flujo eléctrico y el campo eléctrico, están relacionados entre si por la permitividad eléctrica $\varepsilon$
de manera que:

\begin{equation}
		\vec{D} = \varepsilon \vec{E} = \varepsilon_{0} \varepsilon_{r} \vec{E} \label{eq:relacioncampos}
\end{equation}

En donde las variables $\varepsilon_0$ y $\varepsilon_r$, son la permitividad eléctrico del vació y la
permitividad eléctrica relativa del medio.

\subsection{Ley de Gauss}

Si se toma la ecuación \ref{eq:gauss} y se integra en ambos lados, se obtiene que:

\begin{equation}\label{eq:gausspar}
		\int_v \vec{\nabla} \bullet \vec{D} \mathrm{d}v = \int_v \rho_v \mathrm{d}v = Q
\end{equation}

En donde $Q$ es la carga total encerrada en el volumen $v$.

Utilizando el teorema de la divergencia, se puede concluir que:

\begin{equation}
		\int_v \vec{\nabla} \bullet \vec{D} \mathrm{d}v = \oint_s \vec{D} \bullet \mathrm{d}\vec{s}
\end{equation}

Por lo que la ecuación \ref{eq:gausspar}, se transforma en:

\begin{equation}\label{eq:gaussintegral}
		\oint_s \vec{D} \bullet \mathrm{d}\vec{s} = Q
\end{equation}

Al considerar una carga puntual ``$q$'' por simetría, se considera que el flujo eléctrico tiene dirección a lo
largo del radio de la esfera que cerca a ``$q$''; y que el diferencial de superficie también tiene dirección
radial. Entonces $\vec{D}(R) = \hat{R}D_R$ y $\mathrm{d}\vec{s} = \hat{R}\mathrm{d}s$. Al resolver la ecuación
\ref{eq:gaussintegral}, se obtiene:

\begin{align}
		\oint_s \vec{D} \bullet \mathrm{d}\vec{s} &= q \\
		\oint_s \hat{R} D_R \bullet \hat{R} \mathrm{d}s &=q \\
		\oint_s D_R \mathrm{d}s &=q \\
		D_R 4\pi R^2 &=q
\end{align}

Al reemplazar esta solución en la ecuación \ref{eq:relacioncampos} se obtiene una expresión para calcular
el campo eléctrico inducido por una carga puntual.

\begin{equation}\label{eq:coulombpuntual}
		\vec{E} = \hat{R} \frac{q}{4\pi \varepsilon R^2}
\end{equation}

\subsection{Ley de Coulomb}

Si se quisiera conocer el campo producido por más de una carga puntual, se deben considerar las posiciones
y las cargas. La distancia entre el punto del espacio para el cual se quiere determinar el campo y la carga
puntual ``$n$'' se puede expresar como $|\vec{R} - \vec{R}_n|$, mientras que el vector unitario $\vec{R}$
puede expresarse como $(\vec{R} - \vec{R}_n) / |\vec{R} - \vec{R}_n|$. Por lo que, la ecuación \ref{eq:coulombpuntual}
se transforma en:

\begin{equation}\label{eq:coulomb}
		\vec{E} = \frac{q_n (\vec{R} - \vec{R}_n)}{|\vec{R} - \vec{R}_n|^3}
\end{equation}

Por superposición se sabe que el campo total será la suma del efecto de todas las cargas puntuales. Por lo que:

\begin{equation}\label{eq:leydecoulomb}
		\vec{E} = \frac{1}{4\pi \varepsilon} \sum_{i = 1}^{N}\frac{q_i (\vec{R} - \vec{R}_i)}{|\vec{R} - \vec{R}_i|^3}
\end{equation}

Utilizando esta ecuación, se puede decir que la fuerza sobre una carga puntal, puede ser calculada como:

\begin{equation}\label{eq:fuerzacarga}
		\vec{F} = q\vec{E}
\end{equation}

\subsection{Potencial eléctrico}

Si la fuerza sobre una carga en un campo eléctrico está dada por \ref{eq:fuerzacarga}, para mover dicha carga
sin aceleración, se requerirá una fuerza igual en sentido opuesto a la carga (opuesta al campo eléctrico), entonces:

\begin{equation}
		\vec{F}_{ext} =	- \vec{F} = -q\vec{E}
\end{equation}

El trabajo necesario para moverla, estará dado por la derivada sobre el vector diferencial $\mathrm{d}\vec{l}$:

\begin{equation}
		\mathrm{d}W = q\vec{E} \bullet \mathrm{d}\vec{l}
\end{equation}

La energía potencial eléctrica por unidad de carga o potencial eléctrico o voltaje queda definido como:

\begin{equation}\label{eq:defvoltaje}
		\mathrm{d} V \triangleq \frac{\mathrm{d}W}{q} = -\vec{E} \bullet \mathrm{d}\vec{l}
\end{equation}

La diferencia de potencial entre dos puntos $P_1$ y $P_2$ se puede obtener integrando como:

\begin{align}
		\int_{P_1}^{P_2}\mathrm{d}V &= - \int_{P_1}^{P_2}\vec{E} \bullet \mathrm{d}\vec{l} \\
		V_{21} &= - \int_{P_1}^{P_2}\vec{E} \bullet \mathrm{d}\vec{l} \label{eq:voltaje}
\end{align}


\subsection{Capacitancia}\label{sec:capacitancia}

Dos conductores, independiente de su forma, cuando se ven separados por un dieléctrico (aislante), forma un
condensador (o capacitor). Al conectar estos a una fuente de corriente continua, el conductor conectado
al lado positivo de la fuente almacenará una carga $+Q$, mientras que el conectado al lado negativo
almacenará carga $-Q$.

La capacitancia de un condensador se define como la carga almacenada sobre el voltaje entre las placas.

\begin{equation}\label{eq:defcap}
		C = \frac{Q}{V}
\end{equation}

De acuerdo con las ecuaciones  \ref{eq:relacioncampos}, \ref{eq:gaussintegral} y \ref{eq:voltaje}, es posible
escribir la capacitancia como:

\begin{equation}\label{eq:capacitancia}
		C = \frac{\int_s \varepsilon \vec{E} \mathrm{d}\vec{s}}{-\int_l \vec{E} \mathrm{d}\vec{l}}
\end{equation}

Bajo esta ecuación se ve de manera más clara que la capacitancia del condensador no depende del campo
eléctrico, si no que, solo de su geometría y del dieléctrico que los separa.

\subsection{Energía almacenada}

De acuerdo a la ecuación \ref{eq:defvoltaje} es posible obtener una expresión para calcular la energía
almacenada en función del voltaje. De acuerdo a la ecuación \ref{eq:defcap} se puede expresar el voltaje
como:
\begin{equation}
		V = \frac{Q}{C}
\end{equation}

Si se toman los valores diferenciales entonces:

\begin{equation}
		\mathrm{d}V = \frac{q}{C} \mathrm{d}q
\end{equation}

Aplicando la ecuación \ref{eq:defvoltaje}:

\begin{equation}
		\mathrm{d}W = \frac{q}{C} \mathrm{d}q
\end{equation}

Si el condensador alcanza una carga máxima ``$Q$'' entonces:

\begin{align}
		W &= \int_0^Q \frac{q}{C} \mathrm{d}q \\
		W &= \frac{1}{2} \frac{Q^2}{C} \label{eq:varenergia}
\end{align}

Al despejar $Q$ de la ecuación \ref{eq:defcap} y reemplazarlo en \ref{eq:varenergia} se obtiene:

\begin{equation}\label{eq:energiacap}
		W = \frac{1}{2} C V^2
\end{equation}


\section{Simulación de cargas puntuales}

Se desarrollará una serie de problemas, con variaciones en el medio y material donde serán situadas las cargas
o la superficie de estudio. El desarrollo de ambos problemas será comparado con una simulación en el software
``QuickField''.

Se requiere observar el comportamiento del campo eléctrico en una superficie de $0.5 \times 0.5$ [\si{\metre}],
ejercido por tres diferentes cargas, en tres entornos distintos: vació, etanol y agua a $20$ \si{\degreeCelsius}.
En primera instancia se desarrollará algebraicamente para posteriormente ser comparado por la simulación, teniendo
presente el medio de estudio. La ubicación y cargas de las partículas en estudio se muestran en la tabla
\ref{table:valores}. También se requiere calcular el campo eléctrico y fuerza eléctrica sobre una carga puntual ``$q$''
\footnote{Desde este punto la separación decimal queda expresada por un punto (.) y las comas (,) expresan
separación entre coordenadas.}.

\begin{table}[!tb]
		\caption{Valores y posiciones de las cargas}
		\label{table:valores}
		\centering
		\begin{tabular}{| c | c | c | c |}
				\hline
				Nombre de la carga  & Carga [\si{\coulomb}] & Posición [\si{\metre}] 	& Radio [\si{\metre}]	\\
				\hline
				\hline
				$q_1$				& $7 \times 10^{-8}$ 	& $(0.1,0.1)$				& 0.04					\\
				\hline
				$q_2$				& $-5 \times 10^{-8}$	& $(0.15,0.35)$ 			& 0.04					\\
				\hline
				$q_3$ 				& $3 \times 10 ^{-8}$ 	& $(0.4,0.2)$ 				& 0.04					\\
				\hline
				$q$ 				& $5 \times 10^{-8}$ 	& $(0.25,0.25)$				& ---					\\
				\hline
		\end{tabular}
\end{table}

Las propiedades del medio influyen en el campo eléctrico generado por las cargas individuales. Cada medio posee una
permitividad eléctrica distinta por consiguiente el campo eléctrico es dependiente de la permitividad relativa. La
permitividad relativa de cada medio se puede ver en la tabla \ref{table:permitividad}.

\begin{table}[!tb]
		\caption{Permitividad de los materiales}
		\label{table:permitividad}
		\centering
		\begin{tabular}{| c | c |}
				\hline
				Medio material 	& Permitividad Relativa ($\varepsilon_r$)	\\
				\hline
				\hline
				Vació			& $1$										\\
				\hline
				Etanol 			& $24.3$ 									\\
				\hline
				Agua 			& $80.2$									\\
				\hline
		\end{tabular}
\end{table}

\subsection{Análisis de las cargas en distintos medios}

Utilizando estos datos, se construye una simulación en ``QuickField'' para poder observar en gráficos el
comportamiento del campo eléctrico. Los resultados de pueden ver en las figuras \ref{fig:vacio}, \ref{fig:agua}
y \ref{fig:etanol}.

\begin{figure}[!tb]
		\centering
		\includegraphics[width=0.4\textwidth]{imagenes/vacio.png}
		\caption{Campo eléctrico de las cargas en el vacío}
		\label{fig:vacio}
\end{figure}

\begin{figure}[!tb]
		\centering
		\includegraphics[width=0.4\textwidth]{imagenes/agua.png}
		\caption{Campo eléctrico de las cargas en agua}
		\label{fig:agua}
\end{figure}

\begin{figure}[!tb]
		\centering
		\includegraphics[width=0.4\textwidth]{imagenes/etanol.png}
		\caption{Campo eléctrico en etanol}
		\label{fig:etanol}
\end{figure}

Analizando los resultados es apreciable que independiente del medio la distribución del campo eléctrico es
exactamente el mismo. Sin embargo, la magnitud de este varia dependiendo del medio. Para un medio
con una permitividad más baja como el vacío, el campo eléctrico es mucho mayor. Luego, en el medio con mayor
permitividad, el campo se ve disminuido; esto se puede apreciar en las ecuaciones de igual manera, en la
ecuación \ref{eq:leydecoulomb} se ve que existe una relación de proporcionalidad inversa entre el campo
y la permitividad.

\subsection{Fuerza ejercida sobre una carga puntual}

Para el cálculo de la fuerza eléctrica que ejercen las cargas $q_1$, $q_2$ y $q_3$ sobre la carga puntual $q$ es
necesario el cálculo del campo eléctrico. Por el teorema de superposición sabemos que el campo eléctrico total
será la suma del efecto de cada una de las cargas sobre la carga puntual ``$q$''.

\begin{equation}
		\vec{E}_{total} = \vec{E}_{1q} + \vec{E}_{2q} + \vec{E}_{3q}
\end{equation}

El efecto individual de cada campo sobre la carga, se expresa entonces como:

\begin{align*}
		\vec{E}_{1q} &= \frac{q_1 \left(R_q - R_1 \right)}{4 \pi \varepsilon |R_q - R_1|^{3}}  \\
		\vec{E}_{2q} &= \frac{q_2 \left(R_q - R_2 \right)}{4 \pi \varepsilon |R_q - R_2|^{3}}  \\
		\vec{E}_{3q} &= \frac{q_3 \left(R_q - R_3 \right)}{4 \pi \varepsilon |R_q - R_3|^{3}}
\end{align*}

Las diferencias de vectores y sus magnitudes dan:

\begin{align*}
		&R_q - R_1 = 0.25 \hat{x} + 0.25 \hat{y} - 0.1 \hat{x} - 0.1 \hat{y} = 0.15 \hat{x} + 0.15 \hat{y} \\
		&|R_q - R_1|^3 = 9.55 \times 10^{-3} \\
		&R_q - R_2 = 0.25 \hat{x} + 0.25 \hat{y} - 0.15 \hat{x} - 0.35 \hat{y} = 0.1 \hat{x} - 0.1 \hat{y} \\
		&|R_q - R_2|^3 = 2.82 \times 10^{-3} \\
		&R_q - R_3 = 0.25 \hat{x} + 0.25 \hat{y} - 0.4 \hat{x} - 0.2 \hat{y} = -0.15 \hat{x} + 0.05 \hat{y} \\
		&|R_q - R_3|^3 = 3.95 \times 10^{-3}
\end{align*}

El campo eléctrico total expresado como la suma de estos vectores, queda como:

\begin{align}
		\begin{split}
		\vec{E}_{total} &= \frac{1}{4\pi \varepsilon} \Bigg[\frac{7(0.15 \hat{x} + 0.15 \hat{y})}{9.55}
		- \frac{5(0.1 \hat{x} - 0.1 \hat{y})}{2.82} \\
		&+ \frac{3(-0.15 \hat{x} + 0.05 \hat{y})}{3.95} \Bigg] \frac{10^{-8}}{10^{-3}}
		\end{split} \\
		\vec{E}_{total} &= \frac{10^{-5}}{4\pi \varepsilon} (-0.18 \hat{x} + 0.33\hat{y}) \label{eq:etotal}
\end{align}

La fuerza puede ser calculada como el producto entre la carga y el campo eléctrico total. De acuerdo a la
ecuación \ref{eq:fuerzacarga}. La fuerza en la carga puntual ``$q$'' queda entonces, de acuerdo a las
ecuaciones \ref{eq:etotal} y \ref{eq:fuerzacarga}.

\begin{equation}\label{eq:fcarga}
		F = \frac{5 \times 10^{-13}}{4 \pi \varepsilon_r \cdot \varepsilon_0} (-0.18 \hat{x} + 0.33 \hat{y})
\end{equation}

Utilizando la ecuación \ref{eq:fcarga} podemos reemplazar el $\varepsilon_r$ con las permitividades relativas
de los materiales.

Para el vació, con una permitividad de $\varepsilon = 1$, se obtiene que:

\[
		\vec{F}_e = (-8.082 \times 10^{-4} \hat{x} + 1.482 \times 10^{-3} \hat{y}) [\si{\newton}]
\]

Lo cual comparado con el programa es\footnote{Para resolver este ejercicio mediante el uso de un programa
se implementó una rutina en python capaz de resolver el problema de las cargas eléctricas tratando de emular
el comportamiento de ``QuickField''. El código del programa se encuentra en un
\href{https://www.gitlab.com/MaxPow2718r/tarea-campos/-/blob/master/codigo/fuerza.py}{repositorio en GitLab}.}:

\begin{align*}
		\input{codigo/output_vacio.tex}
\end{align*}

Para el etanol, con una permitividad de $\varepsilon = 24.3$, se obtiene

\[
		\vec{F}_e = (-3.330 \times 10^{-5} \hat{x}+ 6.105 \times 10^{-5} \hat{y}) [\si{\newton}]
\]
Comparado con el programa:
\begin{align*}
		\input{codigo/output_etanol.tex}
\end{align*}

Para el agua, con una permitividad $\varepsilon = 80.2$, se tiene que:

\[
		\vec{F} = (-1.009 \times 10^{-5} \hat{x} + 1.848 \times 10^{-5} \hat{y}) \si{\newton}
\]

Comparado con el programa:

\begin{align*}
		\input{codigo/output_agua.tex}
\end{align*}

Es evidente que los cálculos analíticos carecen de precisión decimal, mas son una buena aproximación.

\section{Simulación de placas paralelas}\label{sec:capacitor}

Dos placas paralelas conductoras con dimensiones $0.2 \times 0.2 \times 0.1 [\si{\metre}]$ en el plano $(x,y,z)$
tienen una diferencia de potencial de $5 [\si{\volt}]$. Este escenario presenta un condensador, por
lo que usando las ecuaciones y la teoría expuesta en la sección \ref{sec:capacitancia}, se procede
a realizar los cálculos solicitados y determinar:

\begin{itemize}
		\item La capacitancia.
		\item La energía almacenada.
		\item La variación de voltaje entre las placas.
		\item El voltaje de ruptura dieléctrica.
\end{itemize}
en cuatro distintos medios: aire, papel, vidrio y mica.

Para la geometría del problema, utilizando la ecuación \ref{eq:capacitancia}, es posible determinar una
expresión para el calculo de la capacitancia en función de la geometría de las placas y la permitividad
de los medios.

En particular, el campo eléctrico sobre las placas es $\hat{y} \vec{E}$ y el diferencial de superficie es
$\mathrm{d}\vec{s} = \hat{y}\mathrm{d}x\mathrm{d}z$. Mientras que el campo entre las placas será
$\hat{y} \vec{E}$ y su diferencial de linea $\mathrm{d}\vec{l} = \hat{y} \mathrm{d}y$. Reemplazando
en la ecuación \ref{eq:capacitancia}, se obtiene que;

\begin{equation}
		C = \frac{\varepsilon \int_s \hat{y}E \bullet \hat{y}\mathrm{d}x\mathrm{d}z}
		{\int_l \hat{y}E \bullet \hat{y} \mathrm{d}y}
\end{equation}

\begin{equation}\label{eq:capejercicio}
		C = \frac{\varepsilon E \int_s \mathrm{d}x\mathrm{d}z}{E\int_l \mathrm{d}y}
\end{equation}

La integral del numerador de la ecuación \ref{eq:capejercicio} es claramente el área de la placa,
mientras que su denominador es la distancia entre las placas. Por lo que la ecuación se puede escribir
como:

\begin{equation}\label{eq:capacitanciasimple}
		C = \frac{\varepsilon A}{l}
\end{equation}

En donde $A$ es el área de las placas y $l$ es el largo de las placas.

\begin{table}[!tb]
		\caption{Permitividad relativa de los distintos medios \cite{edwards_2016},\cite{ulaby2007}}
		\label{table:permitividades2}
		\centering
		\begin{tabular}{| c | c |}
				\hline
				Medio	& Permitividad relativa \\
				\hline
				\hline
				Aire	& 1 \\
				\hline
				Papel	& 3 \\
				\hline
				Vidrio	& 4.7 \\
				\hline
				Mica	& 5.7 \\
				\hline
		\end{tabular}
\end{table}

\subsection{Calculo de las capacitancias}

Utilizando la ecuación \ref{eq:capacitanciasimple} y la información de la tabla \ref{table:permitividades2}
es posible obtener los resultados para todos los medios. Estos resultados son presentados en la tabla
\ref{table:resultadoscapacitancias}.

\begin{table}[!tb]
		\caption{Capacitancia de el condensador en los distintos medios}
		\label{table:resultadoscapacitancias}
		\centering
		\begin{tabular}{| c | c |}
				\hline
				Medio	& Capacitancia $[\si{\pico\farad}]$  \\
				\hline
				\hline
				Aire	& 0.8854 \\
				\hline
				Papel	& 2.6562 \\
				\hline
				Vidrio	& 4.1614 \\
				\hline
				Mica	& 4.9582 \\
				\hline
		\end{tabular}
\end{table}

Al intentar recrear los cálculos con aumento y disminución en un $50\%$ en la distancia entre las placas y
con un $50\%$ de aumento y disminución en el area de las placas. Los resultados se pueden ver en la tabla
\ref{table:variacionescapacitancia}

\begin{table}[!tb]
		\caption{Capacitancia con todas las variaciones de área y distancia entre las placas}
		\label{table:variacionescapacitancia}
		\centering
		\begin{tabular}{| c | c | c | c | c | c |}
				\hline
				\multicolumn{2}{|c|}{ } & \multicolumn{4}{c|}{Capacitancia $[\si{\pico\farad}]$}  \\
				\hline
				Área $[\si{\metre} ^2]$ & Distancia $[\si{\metre}]$ & Aire 		& Papel 	& Vidrio 	& Mica 		\\
				\hline
				$0.01$			 		& $0.2$						& $0.44$	& $1.32$	& $1.04$	& $2.52$	\\
				\hline
				$0.02$ 					& $0.1$ 					& $1.77$	& $5.31$ 	& $8.32$ 	& $9.92$	\\
				\hline
				$0.01$			 		& $0.1$ 					& $0.88$ 	& $2.65$ 	& $4.16$ 	& $5.04$ 	\\
				\hline
				$0.03$ 					& $0.2$ 					& $1.32$ 	& $3.98$ 	& $6.24$ 	& $7.57$ 	\\
				\hline
				$0.02$ 					& $0.3$ 					& $0.59$ 	& $1.77$ 	& $2.77$ 	& $3.36$ 	\\
				\hline
				$0.03$ 					& $0.3$ 					& $0.88$ 	& $2.65$ 	& $4.16$ 	& $5.04$ 	\\
				\hline
				$0.03$ 					& $0.1$ 					& $2.66$ 	& $7.97$ 	& $12.50$ 	& $14.90$ 	\\
				\hline
				$0.01$ 					& $0.3$ 					& $0.30$ 	& $0.89$ 	& $1.39$ 	& $1.65$ 	\\
				\hline
		\end{tabular}
\end{table}

Tal como se esperaba ver desde las ecuaciones a medida que la permitividad relativa aumenta, también aumenta
la capacitancia.

\subsection{Energía entra las placas}

Usando la ecuación \ref{eq:energiacap} es posible determinar la energía, usando la información de las tablas
\ref{table:resultadoscapacitancias} y \ref{table:variacionescapacitancia} se pueden obtener todos los valores
de energía para todas las variaciones. Estos resultados pueden verse en la tabla \ref{table:resultadosenergia}.

\begin{table}[!tb]
		\caption{Resultados para los cálculos de energía}
		\label{table:resultadosenergia}
		\begin{tabular}{| c | c | c | c | c | c |}
				\hline
				\multicolumn{2}{|c|}{ } & \multicolumn{4}{c|}{Energía $[\si{\pico\joule}]$}  \\
				\hline
				Área $[\si{\metre} ^2]$ & Distancia $[\si{\metre}]$ & Aire 		& Papel 	& Vidrio 	& Mica 		\\
				\hline
				$0.02$ 					& $0.2$ 					& $11.07$ 	& $33.32$ 	& $52.02$ 	& $61.98$ 	\\
				\hline
				$0.01$			 		& $0.2$						& $5.54$	& $16.60$	& $26.00$	& $31.00$	\\
				\hline
				$0.02$ 					& $0.1$ 					& $21.10$	& $66.40$ 	& $104.00$ 	& $124.00$	\\
				\hline
				$0.01$			 		& $0.1$ 					& $11.10$ 	& $33.20$ 	& $52.00$ 	& $62.00$ 	\\
				\hline
				$0.03$ 					& $0.2$ 					& $16.60$ 	& $49.8$ 	& $78.00$ 	& $93.00$ 	\\
				\hline
				$0.02$ 					& $0.3$ 					& $7.38$ 	& $22.10$ 	& $34.7$ 	& $41.30$ 	\\
				\hline
				$0.03$ 					& $0.3$ 					& $11.10$ 	& $33.20$ 	& $52.00$ 	& $62.00$ 	\\
				\hline
				$0.03$ 					& $0.1$ 					& $33.20$ 	& $99.60$ 	& $156.00$ 	& $186.00$ 	\\
				\hline
				$0.01$ 					& $0.3$ 					& $3.69$ 	& $11.10$ 	& $17.30$ 	& $20.70$ 	\\
				\hline
		\end{tabular}
\end{table}

Estos resultados comparados con las simulaciones en el programa no coinciden del todo. Los gráficos del
campo eléctrico generados se pueden ver en las figuras \ref{fig:simaire}, \ref{fig:simpapel}, \ref{fig:simvidrio} y
\ref{fig:simmica}.

Por otro lado los resultados de energía y capacitancia, obtenidos en las simulaciones se pueden ver en las
imágenes \ref{fig:resaire},  \ref{fig:respapel}, \ref{fig:resvidrio} y \ref{fig:resmica}.

\begin{figure}[!tb]
		\centering
		\includegraphics[width = 0.3\textwidth]{imagenes/resultados_aire}
		\caption{Resultados de simulación para el aire}
		\label{fig:resaire}
\end{figure}

\begin{figure}[!tb]
		\centering
		\includegraphics[width = 0.3\textwidth]{imagenes/resultados_papel}
		\caption{Resultados de simulación para el papel}
		\label{fig:respapel}
\end{figure}
\begin{figure}[!tb]
		\centering
		\includegraphics[width = 0.3\textwidth]{imagenes/resultados_vidrio}
		\caption{Resultados de simulación para el vidrio}
		\label{fig:resvidrio}
\end{figure}
\begin{figure}[!tb]
		\centering
		\includegraphics[width = 0.3\textwidth]{imagenes/resultados_mica}
		\caption{Resultados de simulación para la mica}
		\label{fig:resmica}
\end{figure}


Los resultados de energía se acercan un poco a los obtenidos de manera analítica, pero los resultados
de capacitancia no se acercan en mucho a los obtenidos de manera analítica.

\subsection{Voltaje entre las placas}

Como queda expresado en la introducción de esta sección (\ref{sec:capacitor}) el campo eléctrico entre las
placas está dado por $\hat{y}\vec{E}$ y su diferencial de linea es $\mathrm{d}\vec{l} = \hat{y}\mathrm{d}y$,
sabiendo esto y utilizando la ecuación \ref{eq:voltaje} es posible expresar el voltaje entre las placas como:

\begin{equation}\label{eq:voltajeplacas}
		V = - \int_{0}^{l} \hat{y}E \bullet \hat{y}\mathrm{d}y
\end{equation}

\begin{equation}\label{eq:solucionvoltaje}
		V = - El
\end{equation}

El signo menos en esta ecuación indica que el voltaje en el condensador tiene polaridad opuesta a el
voltaje entregado por la fuente de corriente continua.

Es necesario encontrar una expresión para el campo eléctrico entre las placas. Utilizando la ecuación
\ref{eq:gaussintegral} y transformándola en su equivalente con el campo eléctrico, se obtiene:

\begin{equation}
		\oint_s \vec{E} \bullet \mathrm{d}\vec{s} = \frac{Q}{\varepsilon}
\end{equation}

La superficie que cerca el espacio entra las placas tiene seis superficies. Dos de estas superficies tienen
normal paralela al campo eléctrico, mientras que las otras cuatro tienen una normal perpendicular al campo
eléctrico. Las superficies con normal perpendicular al campo no afectarán a los cálculos puesto que al resolver
el producto punto $\vec{E} \bullet \mathrm{d}\vec{s}$ resultarán ser cero. Por lo que queda:

\begin{equation}
		\int_{s_1} \vec{E} \bullet \mathrm{d}\vec{s_1} + \int_{s_2} \vec{E} \bullet \mathrm{d}\vec{s_2}
		= \frac{Q}{\varepsilon}
\end{equation}

El campo eléctrico como ya ha sido definido es $\hat{y}\vec{E}$, mientras que las superficies son iguales,
$\mathrm{d}\vec{s_1} = \mathrm{d}\vec{s_2} = \hat{y}\mathrm{d}x\mathrm{d}z$, por lo que:

\begin{equation}
		2 \int_s \hat{y}E \bullet \hat{y}\mathrm{d}x\mathrm{d}z = \frac{Q}{\varepsilon}
\end{equation}

Es claro que la integral de superficie es un área paralelo a las placas que se encuentra entre ellas.
La ecuación puede ser reescrita de modo que:

\begin{align}
		2EA &= \frac{Q}{\varepsilon} \\
		E &= \frac{Q}{2 A \varepsilon} \label{eq:solucioncampo}
\end{align}

Al reemplazar la ecuación \ref{eq:solucioncampo}, en la ecuación \ref{eq:solucionvoltaje} se puede obtener
una ecuación para determinar el voltaje producido por el campo de una de las placas.

\begin{equation}\label{eq:voltajedeunaplaca}
		V = \frac{\sigma l}{2\varepsilon}
\end{equation}

Donde $\sigma$ es la densidad de carga superficial. Como las placas almacenan cargas con signo opuesto, los
campos eléctricos producidos por ambas placas será la suma de ambos campos. Por lo que el voltaje entre las
placas queda expresado como:

\begin{equation}\label{eq:voltajeentreplacas}
		V = \frac{\sigma l}{\varepsilon}
\end{equation}

El calculo del voltaje queda expresado en función de la densidad de carga superficial. Los resultados para
todas las variaciones de distancia entre las placas, se puede ver en la tabla \ref{table:resultadosvoltajes}.

\begin{table}[!tb]
		\label{table:resultadosvoltajes}
		\caption{Resultados de voltajes para diferentes distancias}
		\centering
		\begin{tabular}{| c | c | c | c | c |}
				\hline
				& \multicolumn{4}{c |}{Voltaje $\sigma [\si{\mega\volt\coulomb\per\square\metre}]$}	\\
				\hline
				Largo $[\si{\metre}]$	& Aire		& Papel		& Vidrio	& Mica			\\
				\hline
				$0.1$ 					& $5.6$ 	& $1.88$ 	& $1.2$ 	& $0.991$		\\
				\hline
				$0.2$ 					& $11.29$ 	& $3.766$ 	& $2.4$ 	& $1.98$ 		\\
				\hline
				$0.3$ 					& $16.94$ 	& $5.65$ 	& $3.606$ 	& $2.973$ 		\\
				\hline
		\end{tabular}
\end{table}

\subsection{Ruptura dieléctrica}

Pare el calculo de el voltaje de ruptura dieléctrica, se puede utilizar la ecuación \ref{eq:solucionvoltaje}
y los datos de fuerza dieléctrica del campo dados en la tabla \ref{table:ruptura}. Los resultados se pueden
ver en la tabla \ref{table:resultadosruptura}.

\begin{table}[!tb]
		\caption{Campos de ruptura dieléctrica \cite{prasad2007}, \cite{ulaby2007}}
		\label{table:ruptura}
		\centering
		\begin{tabular}{| c | c |}
				\hline
				Medio	& Resistencia dieléctrica $[\si{\mega\volt\per\metre}]$ \\
				\hline
				Aire	& $3$ 	\\
				\hline
				Papel 	& $16$ 	\\
				\hline
				Vidrio	& $26.1$\\
				\hline
				Mica	& $200$ \\
				\hline
		\end{tabular}
\end{table}

\begin{table}[!tb]
		\caption{Resultados de voltaje de ruptura}
		\label{table:resultadosruptura}
		\centering
		\begin{tabular}{| c | c | c | c | c |}
				\hline
							& \multicolumn{4}{c|}{Voltaje de ruptura $[\si{\mega\volt}]$} \\
				\hline
				Largo $[\si{\metre}]$	& Aire		& Papel		& Vidrio	& Mica		\\
				\hline
				$0.1$ 					& $0.3$ 	& $1.6$ 	& $2.61$	& $20$ 		\\
				\hline
				$0.2$ 					& $0.6$ 	& $3.2$ 	& $5.22$ 	& $40$ 		\\
				\hline
				$0.3$ 					& $0.9$ 	& $4.8$ 	& $7.83$ 	& $60$ 		\\
				\hline
		\end{tabular}
\end{table}


\section{Conclusiones}

Dadas las simulaciones realizadas y los cálculos desarrollados a partir de las ecuaciones derivadas
de las ecuaciones de Maxwell, es primordial resaltar que, en este caso particular, las dos ecuaciones
desacopladas sirven de perfecta manera para representar cualquier fenómeno en el ámbito de la
electrostática.

Queda demostrado también que tras el desarrollo de las ecuaciones, la capacitancia de un condensador,
no depende ni del voltaje aplicado, ni del campo eléctrico. Si no que solo depende de su geometría,
esta al ser modificada muestra grandes cambios en la capacitancia y la energía almacenada en el
condensador.

Las simulaciones realizadas para calcular la fuerza ejercida sobre una carga puntual con el software
``QuickField'' no resultaron de buena manera. En su lugar se implementó una rutina de python para
resolver el problema. En la programación de esta se llego a la idea de que la razón por la cual el
resultado con el software sugerido no se acerca a lo calculado es por dos motivos.

\begin{enumerate}
		\item El software depende de cercar en un area el parámetro que se quiere medir (en este caso
				la fuerza), por lo que los resultados van a variar dependiendo de como se cree ese área.
				También cambian dado que se debe poner un radio a las cargas puntuales y dentro de estas
				en la simulación no hay campo. Esto es algo que no ocurre al hacer una implementación
				propia.
		\item Al agregar todas las cargas a la simulación y recalcular los resultados en ``QuickField''
				se esta calculando nuevamente el campo eléctrico en todos los puntos del espacio, por
				lo que al cercar un area no se esta apreciando el efecto de las otras cargas sobre
				la carga puntual ``$q$'', si no que se esta apreciando el efecto de todas las cargas
				incluyendo la carga puntual sobre ese punto del espacio.
\end{enumerate}

Esa es la percepción obtenida de como podría esta trabajando el software a la hora de realizar los cálculos
de fuerza y campo eléctrico, sin embargo, es imposible saber a ciencia cierta si esto es correcto dada la
naturaleza del código fuente del software; razón por la cual no se puede comparar realmente con la implementación
realizada en python.

Respecto a los resultados de simulación de placas paralelas los resultados obtenidos no resultaron ser cercanos
a los calculados. Al realizar simulaciones extras con otras dimensiones se encontró que al ampliar el espacio en
el que se encontraban las placas, los resultados se acercaban a los cálculos. Estos se pueden ver en las figuras
\ref{fig:aire6} y \ref{fig:aire7} se aprecia que al aumentar las dimensiones, la capacitancia disminuye. De aquí
nace la suposición de que si el espacio en la simulación fuera infinito el resultado convergería eventualmente
a lo calculado analíticamente.

\appendices

\section{Resultados Extras de las simulaciones}

Al realizar las simulaciones, se obtuvieron una gran cantidad
de gráficas, las cuales por no extender en demasía el artículo se optaron por agregar en los apéndices.

Las simulaciones originalmente se realizaron en un espacio de $3.2 \times 3.2 [\si{\metre}]$. En intentos
más tardíos se intentó realizar la simulación con dimensiones diferentes. Con esto se encontró que
al aumentar estas dimensiones los resultados convergían lentamente hacia los valores calculados analíticamente.
Los resultados de estas simulaciones posteriores se puede ver en las figuras \ref{fig:aire6} y \ref{fig:aire7}.

\begin{figure}[!tb]
		\centering
		\includegraphics[width = 0.3\textwidth]{imagenes/resultado_aire_6m}
		\caption{Resultados para la simulación en aire con dimensiones de $6\times 6 [\si{\metre}]$}
		\label{fig:aire6}
\end{figure}


\begin{figure}[!tb]
		\centering
		\includegraphics[width = 0.3\textwidth]{imagenes/resultado_aire_7m}
		\caption{Resultados para la simulación en aire con dimensiones de $7\times 7 [\si{\metre}]$}
		\label{fig:aire7}
\end{figure}

\begin{figure}[!tb]
		\centering
		\includegraphics[width=0.4\textwidth]{imagenes/simulacion_aire}
		\caption{Simulación de placas paralelas en el aire}
		\label{fig:simaire}
\end{figure}

\begin{figure}[!tb]
		\centering
		\includegraphics[width=0.4\textwidth]{imagenes/simulacion_papel}
		\caption{Simulación de placas paralelas en papel}
		\label{fig:simpapel}
\end{figure}


\begin{figure}[!tb]
		\centering
		\includegraphics[width=0.4\textwidth]{imagenes/simulacion_vidrio}
		\caption{Simulación de placas paralelas en vidrio}
		\label{fig:simvidrio}
\end{figure}
\begin{figure}[!tb]
		\centering
		\includegraphics[width=0.4\textwidth]{imagenes/simulacion_mica}
		\caption{Simulación de placas paralelas en mica}
		\label{fig:simmica}
\end{figure}


\begin{figure}[!tb]
		\centering
		\includegraphics[width=0.4\textwidth]{imagenes/simulacion_aire_6m}
		\caption{Simulación de placas paralelas en el aire en un espacio de $6\times 6[\si{\metre}]$}
		\label{fig:simaire6}
\end{figure}

\begin{figure}[!tb]
		\centering
		\includegraphics[width=0.4\textwidth]{imagenes/simulacion_aire_7m}
		\caption{Simulación de placas paralelas en el aire en un espacio de $7\times 7[\si{\metre}]$}
		\label{fig:simaire7}
\end{figure}

Otra alternativa es cercar las placas paralelas, en la simulación, solo al volumen que ocupaban. Esto
general un resultado mucho más parecido al calculado analíticamente. Lo cual lleva a pensar que, al ser
la distancia entre las placas casi de la misma dimension que los largos de estas, el programa integra el
campo eléctrico en el área al rededor de las placas en vez de calcular la capacitancia a través de la
geometría del capacitor. Esto lleva a generar estas diferencias, ya que como se comentó, la distancia
entre las placas es de casi la misma proporción que el largo de estas; debido a esto el campo eléctrico
se curva fuera de las placas. Mientras que al cercar el área al rededor de las placas solo al largo de las
placas y la distancia entre ellas se evita este efecto secundario, lo cual supondría una especie de condición
ideal al calculo de la capacitancia para el programa. Los resultados se pueden ver en las imágenes
\ref{fig:altaire}, \ref{fig:altpapel}, \ref{fig:altvidrio} y \ref{fig:altmica}.

\begin{figure}[!tb]
		\centering
		\includegraphics[width=0.4\textwidth]{imagenes/altaire}
		\caption{Simulación alternativa en aire}
		\label{fig:altaire}
\end{figure}


\begin{figure}[!tb]
		\centering
		\includegraphics[width=0.4\textwidth]{imagenes/altpapel}
		\caption{Simulación alternativa en papel}
		\label{fig:altpapel}
\end{figure}

\begin{figure}[!tb]
		\centering
		\includegraphics[width=0.4\textwidth]{imagenes/altvidrio}
		\caption{Simulación alternativa en vidrio}
		\label{fig:altvidrio}
\end{figure}

\begin{figure}[!tb]
		\centering
		\includegraphics[width=0.4\textwidth]{imagenes/altmica}
		\caption{Simulación alternativa en mica}
		\label{fig:altmica}
\end{figure}


\section{Comparación del software ``QuickField'' con el desarrollo matemático programado}

Entre los resultados esperados, la simulación con ``QuickField'' para la la fuerza sobre una carga
puntual, no se acerca a los resultados obtenidos de manera analítica. En principio se pensó que
los resultados analíticos estaban incorrectos, sin embargo, tras un par de veces recalculando
se llego a la conclusión de que el resultado de fuerza entregado por el software no era consistente
con lo que se quería desarrollar. Una opción rápida para suplementar la necesidad de comparar con un
programa los cálculos ya hechos, fue escribir un pequeño
\href{https://www.gitlab.com/MaxPow2718r/tarea-campos/-/blob/master/codigo/fuerza.py}{scrip de python}
que resolviera el problema. Con este se llego a determinar un resultado similar al calculado
de manera analítica. La forma en que el scrip funciona es tomando los valores de las cargas dadas,
tomando las posiciones de estas y; en una matriz pre establecida, calcula los valores del campo para cada
punto del espacio dentro de la matriz. De paso el programa es capaz de graficar el campo entre las cuatro
cargas puntuales, la gráfica puede verse en la figura \ref{fig:python}.

\begin{figure}
		\centering
		\includegraphics[width=0.4\textwidth]{codigo/campo.png}
		\caption{Gráfica producida en python}
		\label{fig:python}
\end{figure}

Si bien el resultado no es tan pulcro como el obtenido en ``QuickField'' puede servir como una buena
referencia.


\printbibliography


\end{document}
